class Component {
  static list = [];

  static init(cars) {
    this.list = cars.map((i) => new this(i));
  }

  constructor({
    id,
    plate,
    manufacture,
    model,
    image,
    rentPerDay,
    capacity,
    description,
    transmission,
    available,
    type,
    year,
    options,
    specs,
    availableAt,
    withDriver,
  }) {
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
    this.withDriver = withDriver
    if (this.constructor === Component) {
      throw new Error("Error")
    }
  }

  render() {
    return `
    <div class="col-4">
    <div class="car-list container mt-5">
    <div class="card" style="height: 650px;">
    <img src="${this.image}" class="card-img-top" style="height: 250px;">
    <div class="card-body style ="height: 200px; width: 20px">
        <p><b>${this.model}</b></p>
        <p><b>Rp.${this.rentPerDay}/Hari</b></p>
        <p>${this.description}</p>
        <p><img src="./assets/img/icon-user-car.svg"><span>&ensp;${this.capacity}orang</span></p>
        <p><img src="./assets/img/icon-gear-car.svg"><span>&ensp;${this.transmission}</span></p>
        <p><img src="./assets/img/icon-year-car.svg"><span>&ensp;Tahun ${this.year}</span></p>
        <a href="#" class = "btn btn-success w-100">Pilih Mobil</a>
    </div>
    </div>
</div>
</div>
`;

  }


}

class Car extends Component {

}